duke-shibboleth-container
=========================


First pass at a Shibboleth-only container, running as a non-root user.  Works for Docker, Kubernetes, and OpenShift.

*Customizations*

*  Runs as a non-root user; userid can be random (eg: as would be in OpenShift)
*  Logs to stdout of the container
*  Designed to run *just* the Shibboleth daemon
*  Expects to share the Shibboleth socket with an Apache container with mod_shib installed


## Running with Docker/Docker-Compose

Running this container with Docker or Docker Compose requires a few volume mounts from the host:

*   A volume mount for any config file you want to customize in /etc/shibboleth
*   Volume mounts for each SP TLS key and certificate
*   A volume mount for /var/run/shibboleth/shibd.sock, shared with an Apache container with mod_shib installed.

An example Docker Compose config might resemble:

```
# Docker Compose
services:
  shibd:
    image: duke-shibboleth-container
    volumes:
      - shibboleth2.xml:/etc/shibboleth/shibboleth2.xml:ro
      - attribute-map.xml:/etc/shibboleth/attribute-map.xml:ro
      - sp.key:/etc/pki/tls/private/sp.key:ro
      - sp.crt:/etc/pki/tls/certs/sp.crt:ro
      - shibd.sock:/var/run/shibboleth/shib.sock
```

## Config File mounting with Kubernetes/OpenShift

The normally-documented way of mounting files from a Kubernetes configMap into a volume involves overwritting the directory into which the volume is mounted.  For example, following most documented examples to mount the shibboleth2.xml file into /etc/shibboleth would end up with /etc/shibboleth empty except for the shibboleth2.xml file.

However, if you create a volume for each file you need to mount from the configMap, and mount them with the specific mountPath to the file, specifying the subPath as the file name as well, the files are mounted individually into the directory.  (Yes, it hurt me to type this as much as it hurt you to read it.  See the example below...)

You need a volume for *each* file (the key from the configMap), and a volume mount for each volume.

Example:

```
# Given a configMap named shib-config that contains the keys "shibboleth2.xml" and "attribute-map.xml" - in the containers spec:

containers:
  - volumeMounts:
    - mountPath: /etc/yum/shibboleth2.xml
      name: volume-shib2xml
      subPath: shibboleth2.xml
    - mountPath: /etc/yum/attribute-map.xml
      name: volume-attributemap
      subPath: attribute-map.xml
    volumes:
    - configMap:
        defaultMode: 420
        name: shib-config
      name: volume-shib2xml
    - configMap:
        defaultMode: 420
        name: shib-config
      name: volume-attributemap
```

## Shibboleth socket consideration for Kubernetes/OpenShift

As mentioned above, this container needs to run in the same pod as an Apache contianer, because it must share the Shibboleth socket as a volume mount with Apache.  Both contianers need to mount /var/run/shibboleth/shibd.sock (preferably of type [emptyDir](https://kubernetes.io/docs/concepts/storage/volumes/#emptydir) to be able to talk back and forth.
